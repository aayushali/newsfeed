let state = {
  miniState: false,
  news: [],
  loading: true,
  upVoted: [],
  readNews: [],
  bookmarks: [],
  mostVotedNews: [],
  filter: '',
  serverPagination: {
    page: 1,
    rowsNumber: 0,
    totalPage: 0,
    sortBy: null,
    descending: false,
    rowsPerPage: 8,
  },
};

export default state;
