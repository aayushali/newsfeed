import axios from "axios";
import {LocalStorage, Notify} from "quasar";
import {axiosInstance} from "boot/axios";
import {api} from "boot/axios";

const FETCH_NEWS = '/api/fetchNews';
const VIEW_COUNT = '/api/views-count';
const VOTES_STORE = '/api/votes/store';
const FEEDBACK_URL = '/api/feedback/store';
const MOST_VOTED_URL = '/api/most-upvoted';
let actions = {
  fetchNews({commit, state, getters}) {
    // {commit, state}
    const bookmarkedNews = getters.getBookmarkedNewsId;
    const pagination = state.serverPagination;
    const query = state.filter;
    api.get(`${FETCH_NEWS}?page=${pagination.page}&rowsPerPage=${pagination.rowsPerPage}&sortBy=${pagination.sortBy}&descending=${pagination.descending}&query=${query}`)
      .then(res => {
        let newsData = state.news;
        let data = res.data.data;
        let totalNews = [...newsData, ...data];
        let newNews = totalNews.map(val => {
          if (bookmarkedNews && bookmarkedNews.includes(val.id)) {
            return {
              ...val, isBookmarked: true
            }
          } else {
            return {...val}
          }
        })
        commit('SET_NEWS', newNews);
        commit('SET_TOTAL_NUMBER', res.data.meta.total);
        commit('SET_TOTAL_PAGE', res.data.meta.last_page);
        commit('SET_LOADING', false)
      })
      .catch(error => {
        // Loading.hide();
        // commit('RECORD_ERRORS', error.response.data.errors);
      });
  },

  countViews({commit}, data) {
    api.post(`${VIEW_COUNT}/${data}`)
      .then(res => {
        // console.log(res.data)
      })
      .catch(err => {
        // console.log(err.response)
      })
  },

  upVote({commit}, newsId) {
    api.get(`${VOTES_STORE}/${newsId}`)
      .then(res => console.log(res.data))
      .catch(err => console.log(err.response.data))
  },

  setBookmarks({commit}, data) {
    commit('SET_BOOKMARK', data);
  },

  sendFeedback({commit}, data) {
    api.post(`${FEEDBACK_URL}`, data)
      .then(res => Notify.create({
        message: res.data.message, color: "positive", position: "bottom-right"
      }))
      .catch(err => Notify.create({
        message: err.response.data.message, color: "negative"
      }))
  },

  mostUpvoted({commit, state, getters}) {
    // {commit, state}
    const bookmarkedNews = getters.getBookmarkedNewsId;
    const pagination = state.serverPagination;
    const query = state.filter;
    api.get(`${MOST_VOTED_URL}?page=${pagination.page}&rowsPerPage=${pagination.rowsPerPage}&sortBy=${pagination.sortBy}&descending=${pagination.descending}&query=${query}`)
      .then(res => {
        let newsData = state.mostVotedNews;
        let data = res.data.data;
        let totalNews = [...newsData, ...data];
        let newNews = totalNews.map(val => {
          if (bookmarkedNews && bookmarkedNews.includes(val.id)) {
            return {
              ...val, isBookmarked: true
            }
          } else {
            return {...val}
          }
        });

        commit('SET_MOST_VOTED_NEWS', newNews);
        commit('SET_TOTAL_NUMBER', res.data.meta.total);
        commit('SET_TOTAL_PAGE', res.data.meta.last_page);
        commit('SET_LOADING', false)
      })
      .catch(error => {
        // Loading.hide();
        // commit('RECORD_ERRORS', error.response.data.errors);
      });
  },


}
export default actions;
