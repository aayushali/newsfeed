import {LocalStorage} from "quasar";

let getters = {
  getMiniState: state => {
    return state.miniState;
  },
  getLoadingState: state => {
    return state.loading;
  },
  getNews: state => {
    return state.news
  },
  getReadNews: state => {
    return state.readNews
  },
  getBookmarkedNewsId: () => {
    let news = LocalStorage.getItem('bookmark');
    return news && news.map(val => val.id)
  },
  getMostVotedData: state => {
    return state.mostVotedNews
  },
  getBookmarks: state => {
    return state.bookmarks.map(val => val.id);
  },
  getupVotedData: (state) => {
    return state.upVoted;
  }
}
export default getters;
