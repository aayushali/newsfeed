import {LocalStorage} from "quasar";

const mutations = {
  SET_NEWS(state, news) {
    return state.news = news;
  },
  SET_MOST_VOTED_NEWS(state, news) {
    return state.mostVotedNews = news;
  },
  SET_FILTER(state, query) {
    return state.filter = query;
  },
  SET_BOOKMARK(state, news) {
    state.bookmarks.push(news);
    LocalStorage.set("bookmark", state.bookmarks);
  },
  SET_LOADING(state, value) {
    return state.loading = value;
  },
  SET_UPVOTE(state, value) {
    return state.upVoted.push(value.id)
  },
  REMOVE_READ_NEWS(state, id) {
    let index = state.readNews.findIndex(item => item.id === id);
    state.readNews.splice(index, 1);
  },
  SET_READ_NEWS(state, value) {
    return state.readNews.push(value);
  },

  SET_TOTAL_NUMBER(state, item) {
    state.serverPagination.rowsNumber = item;
  },
  SET_TOTAL_PAGE(state, item) {
    state.serverPagination.totalPage = item;
  },
  SET_SERVER_PAGINATION(state, item) {
    state.serverPagination = item;
  },

}

export default mutations;
