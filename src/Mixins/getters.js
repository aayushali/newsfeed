import {mapGetters} from "vuex";
import {LocalStorage} from "quasar";

export default {
  computed: {
    ...mapGetters("CommonStore", {
      loading: 'getLoadingState',
      newsList: 'getNews',
      getBookmarkedNewsId: 'getBookmarkedNewsId',
      getReadNews: 'getReadNews',
      upVotedData: 'getupVotedData',
      bookmarks: 'getBookmarks',
      mostVotedNews: 'getMostVotedData'
    }),
  }
}
