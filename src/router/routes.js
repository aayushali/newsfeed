const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "/popular",
        component: () => import("pages/TopNews.vue")
      },
      {
        path: "/top-news",
        component: () => import("pages/TopNews.vue")
      },
      {
        path: "/search",
        component: () => import("pages/Search.vue")
      },
      {
        path: "/most-upvoted",
        component: () => import("pages/MostUpvoted.vue")
      },
      {
        path: "/bookmarks",
        component: () => import("pages/Bookmarks.vue")
      },
      {
        path: "/reading-history",
        component: () => import("pages/ReadingHistory.vue")
      },
      {
        path: "/feedback",
        component: () => import("pages/Feedback.vue")
      }
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/Error404.vue"),
  },
];

export default routes;
